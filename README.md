# CoursesAPI pavyzdinis

## Naujausi pakeitimai 0.0.8
* OngoingCourse kontroleris, servisai ir testai sukurti
* Program.cs įdėtas konfigūracija UseLazyLoadingProxies - tam reikia papildomai įsidiegti nuget package į CoursesAPI projektą Microsoft.EntityFrameworkCore.Proxies.
* Papildytos OngoingCourseLine ir OngoingCourse virtualiomis sąvybėmis, į juos automatiškai įkraunami įrašai iš lentelių su kuriomis yra sąryšis

## Getting started

Jeigu norite paleisti projektą lokaliai: Jums reikalingas nuosavas appsettings.json projekte ConsoleApi ir duomenų bazė, kurią galite įsidėti į LocalDatabase projektą arba kitur arba naudoti bet kokia kitą MS SQL duomenų bazę.

![Sąryšiai tarp lentelių](docs/003version1.png)

## Ankstesni pakeitimai

## Ankstesni pakeitimai 0.0.8
* Courses, OngoingCourse, OngoingCourseLine entities ir repozitorijos sukurta
* Courses modeliai, servisai, dekoratoriai ir kontroleris sukurtas. Pridėti testai.
* Ištaisytos kitos klaidos

## Ankstesni pakeitimai 0.0.6
* Lecturer servisai, dekoratoriai ir kontroleris sukurtas. Pridėti testai.
* Pridėta extension klasė ServiceCollectionExtensions, skirta servisams, repozitorijoms ir kt. registracijoms. Naudojam Program.cs dėl skaitomumo.
* Ištaisytas bugas Middleware, nes neveikė POST, PUT metodai. Dabar veikia.

## Ankstesni pakeitimai 0.0.5
* Middleware užregistruotas ir idėta json validavimo logika
* StudentDto papildytas su Age. Age yra apskaičiuojamas iš studento gimimo datos

## Ankstesni pakeitimai 0.0.4
* Pridėtas DEMO GUI projektas
* GUI projektui sukurtas funkcionalumas rodyti studentų sąrašą, sukurti, isštrinti ir pakeisti studentą

## Ankstesni pakeitimai 0.0.3
* Visų Student servisų sutvarkymas su Dto modeliais
* Dekoratorių pristatymas visiems Student servisams
* Middleware pristatymas klaidoms gaudyti iki kontrolerio ir po controlerio (dar bus patobulinta)
* Testų sutvarkymas visiems Student servisams

## Ankstesni pakeitimai 0.0.2
* Perkelti parametrai į Models.Students namespace
* Sukurti Dto modeliai Models.Students kurie skirti biznio logikai ir naudojami tik service klasėse ir tai modeliai kuriuos gražina kontroleris
* Students sukurimo service idetas automatinis createdAt sugeneravimas
* Try catch blokai perkelti į service klases
* Kontroleryje tik tikrinamas statusas Result tipo savybės status
* Sukurti CreateStudentService testai

### Ankstesni pakeitimai 0.0.1
* Sukurta repozitorija ir perkeltas projektas iš kurso repozitorijos

### Ankstesni pakeitimai 0.0.0
* Nėra