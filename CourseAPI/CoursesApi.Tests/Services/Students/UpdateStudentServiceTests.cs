﻿using CoursesApi.Models.Students.Dtos;
using CoursesApi.Models.Students.Parameters;
using DataAccess.Entities;
using DataAccess;
using Moq;
using CoursesApi.Services.Students;

namespace CoursesApi.Tests.Services.Students
{
    [TestClass]
    public class UpdateOngoingCourseServiceTests
    {
        private UpdateStudentParameter parameter = null!;
        private Student studentRepositoryUpdateResult = null!;
        private Student studentRepositoryGetResult = null!;
        private Mock<IRepository<Student>> studentRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            var updateStudentDto = new UpdateStudentDto
            {
                // Initialize with test data
            };
            parameter = new UpdateStudentParameter(updateStudentDto);
            studentRepositoryUpdateResult = CreateStudent();
            studentRepositoryGetResult = CreateStudent();
            studentRepositoryMock = new Mock<IRepository<Student>>();

            studentRepositoryMock
                .Setup(studentRepository => studentRepository.GetAsync(It.IsAny<int>()))
                .ReturnsAsync(studentRepositoryGetResult);

            studentRepositoryMock
                .Setup(repo => repo.UpdateAsync(It.IsAny<Student>()))
                .ReturnsAsync(studentRepositoryGetResult);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsUpdatedStudentDto()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(studentRepositoryUpdateResult.Id, result.Data.Id);
            Assert.AreEqual(studentRepositoryUpdateResult.Name, result.Data.Name);
        }

        [TestMethod]
        public async Task CallsStudentRepositoryUpdateOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            studentRepositoryMock.Verify(s => s.UpdateAsync(It.IsAny<Student>()), Times.Once);
        }

        private UpdateStudentService CreateService()
        {
            return new UpdateStudentService(studentRepositoryMock.Object);
        }

        private Student CreateStudent()
        {
            return new Student();
        }
    }
}
