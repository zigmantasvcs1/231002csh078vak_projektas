﻿using CoursesApi.Models.Students.Parameters;
using CoursesApi.Services.Students;
using DataAccess;
using DataAccess.Entities;
using Moq;
using System.Linq.Expressions;

namespace CoursesApi.Tests.Services.Students
{
    [TestClass]
    public class ListStudentServiceTests
    {
        private ListStudentParameter parameter = null!;
        private List<Student> studentRepositoryResult = null!;
        private Mock<IRepository<Student>> studentsRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new ListStudentParameter(5);
            studentRepositoryResult = CreateStudentsList();
            studentsRepositoryMock = new Mock<IRepository<Student>>();
            studentsRepositoryMock
                .Setup(repo => repo.ListAsync(It.IsAny<int>(), It.IsAny<Expression<Func<Student, bool>>>()))
                .ReturnsAsync(studentRepositoryResult);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsListOfStudents()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(studentRepositoryResult.Count, result.Data.Count);
            for (int i = 0; i < studentRepositoryResult.Count; i++)
            {
                Assert.AreEqual(studentRepositoryResult[i].Id, result.Data[i].Id);
                Assert.AreEqual(studentRepositoryResult[i].Name, result.Data[i].Name);
                Assert.AreEqual(studentRepositoryResult[i].Surname, result.Data[i].Surname);
                Assert.AreEqual(studentRepositoryResult[i].Email, result.Data[i].Email);
                Assert.AreEqual(studentRepositoryResult[i].BirthDay, result.Data[i].BirthDay);
                Assert.AreEqual(studentRepositoryResult[i].DocumentNumber, result.Data[i].DocumentNumber);
            }
        }

        [TestMethod]
        public async Task CallsStudentsRepositoryListOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            studentsRepositoryMock.Verify(
                s => s.ListAsync(parameter.Limit, null),
                Times.Once
            );
        }

        private ListStudentService CreateService()
        {
            return new ListStudentService(studentsRepositoryMock.Object);
        }

        private List<Student> CreateStudentsList()
        {
            return new List<Student>
            {
                new(),
            };
        }
    }
}
