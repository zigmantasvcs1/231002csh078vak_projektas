﻿using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Services.Lecturers;
using DataAccess;
using DataAccess.Entities;
using Moq;
using System.Linq.Expressions;

namespace LecturersApi.Tests.Services.Lecturers
{
    [TestClass]
    public class ListLecturerServiceTests
    {
        private ListLecturerParameter parameter = null!;
        private List<Lecturer> lecturerRepositoryResult = null!;
        private Mock<IRepository<Lecturer>> lecturersRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new ListLecturerParameter(5);
            lecturerRepositoryResult = CreateLecturersList();
            lecturersRepositoryMock = new Mock<IRepository<Lecturer>>();
            lecturersRepositoryMock
                .Setup(repo => repo.ListAsync(It.IsAny<int>(), It.IsAny<Expression<Func<Lecturer, bool>>>()))
                .ReturnsAsync(lecturerRepositoryResult);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsListOfLecturers()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(lecturerRepositoryResult.Count, result.Data.Count);
            for (int i = 0; i < lecturerRepositoryResult.Count; i++)
            {
                Assert.AreEqual(lecturerRepositoryResult[i].Id, result.Data[i].Id);
                Assert.AreEqual(lecturerRepositoryResult[i].Name, result.Data[i].Name);
                Assert.AreEqual(lecturerRepositoryResult[i].Surname, result.Data[i].Surname);
                Assert.AreEqual(lecturerRepositoryResult[i].Email, result.Data[i].Email);
                Assert.AreEqual(lecturerRepositoryResult[i].DocumentNumber, result.Data[i].DocumentNumber);
            }
        }

        [TestMethod]
        public async Task CallsLecturersRepositoryListOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturersRepositoryMock.Verify(
                s => s.ListAsync(parameter.Limit, null),
                Times.Once
            );
        }

        private ListLecturerService CreateService()
        {
            return new ListLecturerService(lecturersRepositoryMock.Object);
        }

        private List<Lecturer> CreateLecturersList()
        {
            return new List<Lecturer>
            {
                new(),
            };
        }
    }
}
