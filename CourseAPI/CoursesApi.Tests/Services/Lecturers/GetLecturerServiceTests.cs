﻿using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Services.Lecturers;
using DataAccess;
using DataAccess.Entities;
using Moq;

namespace CoursesApi.Tests.Services.Lecturers
{
    [TestClass]
    public class GetLecturerServiceTests
    {
        private GetLecturerParameter parameter = null!;
        private Lecturer lecturerRepositoryResult = null!;

        private Mock<IRepository<Lecturer>> lecturerRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateGetLecturerParameter();

            lecturerRepositoryResult = CreateLecturer();

            lecturerRepositoryMock = GetLecturerRepositoryMock();
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await service.CallAsync(null));
        }

        [TestMethod]
        public async Task CallsLecturerRepositoryOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturerRepositoryMock.Verify(s => s.GetAsync(parameter.Id), Times.Once);
            lecturerRepositoryMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task ResultOfServiceContainsValuesFromLecturerRepository()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(lecturerRepositoryResult.Id, result.Data.Id);
            Assert.AreEqual(lecturerRepositoryResult.Name, result.Data.Name);
            Assert.AreEqual(lecturerRepositoryResult.Surname, result.Data.Surname);
            Assert.AreEqual(lecturerRepositoryResult.Email, result.Data.Email);
            Assert.AreEqual(lecturerRepositoryResult.DocumentNumber, result.Data.DocumentNumber);
        }

        private GetLecturerService CreateService()
        {
            return new GetLecturerService(lecturerRepositoryMock.Object);
        }

        private GetLecturerParameter CreateGetLecturerParameter()
        {
            return new GetLecturerParameter(1);
        }

        private Lecturer CreateLecturer()
        {
            return new Lecturer()
            {
                Id = 111,
                Name = "test",
                Surname = "testinukas",
                Email = "testinukas@gmail.com",
                DocumentNumber = "AAA001",
                CreatedAt = new DateTime(2023, 12, 10)
            };
        }

        private Mock<IRepository<Lecturer>> GetLecturerRepositoryMock()
        {
            var mock = new Mock<IRepository<Lecturer>>();

            mock.Setup(repo => repo.GetAsync(It.IsAny<int>())).ReturnsAsync(lecturerRepositoryResult);

            return mock;
        }
    }
}
