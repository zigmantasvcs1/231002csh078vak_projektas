﻿using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Services.Lecturers;
using DataAccess;
using DataAccess.Entities;
using Moq;

namespace LecturersApi.Tests.Services.Lecturers
{
    [TestClass]
    public class UpdateLecturerServiceTests
    {
        private UpdateLecturerParameter parameter = null!;
        private Lecturer lecturerRepositoryUpdateResult = null!;
        private Lecturer lecturerRepositoryGetResult = null!;
        private Mock<IRepository<Lecturer>> lecturerRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            var updateLecturerDto = new UpdateLecturerDto
            {
                // Initialize with test data
            };
            parameter = new UpdateLecturerParameter(updateLecturerDto);
            lecturerRepositoryUpdateResult = CreateLecturer();
            lecturerRepositoryGetResult = CreateLecturer();
            lecturerRepositoryMock = new Mock<IRepository<Lecturer>>();

            lecturerRepositoryMock
                .Setup(lecturerRepository => lecturerRepository.GetAsync(It.IsAny<int>()))
                .ReturnsAsync(lecturerRepositoryGetResult);

            lecturerRepositoryMock
                .Setup(repo => repo.UpdateAsync(It.IsAny<Lecturer>()))
                .ReturnsAsync(lecturerRepositoryGetResult);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsUpdatedLecturerDto()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(lecturerRepositoryUpdateResult.Id, result.Data.Id);
            Assert.AreEqual(lecturerRepositoryUpdateResult.Name, result.Data.Name);
        }

        [TestMethod]
        public async Task CallsLecturerRepositoryUpdateOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturerRepositoryMock.Verify(s => s.UpdateAsync(It.IsAny<Lecturer>()), Times.Once);
        }

        private UpdateLecturerService CreateService()
        {
            return new UpdateLecturerService(lecturerRepositoryMock.Object);
        }

        private Lecturer CreateLecturer()
        {
            return new Lecturer();
        }
    }
}
