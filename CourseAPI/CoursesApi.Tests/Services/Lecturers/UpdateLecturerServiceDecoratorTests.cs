﻿using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services.Lecturers;

namespace LecturersApi.Tests.Services.Lecturers
{
    [TestClass]
    public class UpdateLecturerServiceDecoratorTests
    {
        private UpdateLecturerParameter parameter = null!;
        private Mock<IService<UpdateLecturerParameter, LecturerDto>> updateLecturerServiceMock = null!;
        private Mock<ILogger<UpdateLecturerServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateUpdateLecturerParameter();

            updateLecturerServiceMock = new Mock<IService<UpdateLecturerParameter, LecturerDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            updateLecturerServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<LecturerDto>(200, new LecturerDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            updateLecturerServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            updateLecturerServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private UpdateLecturerServiceDecorator CreateService()
        {
            return new UpdateLecturerServiceDecorator(
                updateLecturerServiceMock.Object,
                loggerMock.Object
            );
        }

        private UpdateLecturerParameter CreateUpdateLecturerParameter()
        {
            return new UpdateLecturerParameter(new UpdateLecturerDto());
        }

        private Mock<ILogger<UpdateLecturerServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<UpdateLecturerServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
