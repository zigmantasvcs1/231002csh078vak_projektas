﻿using CoursesApi.Models.OngoingCourses.Parameters;
using DataAccess.Entities;
using DataAccess;
using Moq;
using CoursesApi.Services.OngoingCourses;

namespace CoursesApi.Tests.Services.OngoingCourses
{
    [TestClass]
    public class DeleteOngoingCourseServiceTests
    {
        private DeleteOngoingCourseParameter parameter = null!;
        private Mock<IRepository<OngoingCourse>> ongoingCourseRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new DeleteOngoingCourseParameter(1);
            ongoingCourseRepositoryMock = new Mock<IRepository<OngoingCourse>>();
        }

        private DeleteOngoingCourseService CreateService()
        {
            return new DeleteOngoingCourseService(ongoingCourseRepositoryMock.Object);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsSuccessResultWhenDeleteIsSuccessful()
        {
            // arrange
            ongoingCourseRepositoryMock.Setup(repo => repo.DeleteAsync(It.IsAny<int>())).ReturnsAsync(true);
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
        }

        [TestMethod]
        public async Task ReturnsNotFoundResultWhenDeleteFails()
        {
            // arrange
            ongoingCourseRepositoryMock
                .Setup(repo => repo.DeleteAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(404, result.Status);
            Assert.IsNull(result.Data);
        }

        [TestMethod]
        public async Task CallsOngoingCourseRepositoryDeleteOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            ongoingCourseRepositoryMock.Verify(s => s.DeleteAsync(parameter.Id), Times.Once);
        }
    }
}
