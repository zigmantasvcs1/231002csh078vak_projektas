﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Dtos;
using CoursesApi.Models.Courses.Parameters;

namespace CoursesApi.Services.Courses
{
    public class CreateCourseServiceDecorator : IService<CreateCourseParameter, CourseDto>
    {
        private readonly IService<CreateCourseParameter, CourseDto> _createCourseService;
        private readonly ILogger<CreateCourseServiceDecorator> _logger;

        public CreateCourseServiceDecorator(
            IService<CreateCourseParameter, CourseDto> createCourseService,
            ILogger<CreateCourseServiceDecorator> logger)
        {
            _createCourseService = createCourseService;
            _logger = logger;
        }

        public async Task<Result<CourseDto>> CallAsync(CreateCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _createCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<CourseDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
