﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;

namespace CoursesApi.Services.OngoingCourses
{
    public class CreateOngoingCourseServiceDecorator : IService<CreateOngoingCourseParameter, OngoingCourseDto>
    {
        private readonly IService<CreateOngoingCourseParameter, OngoingCourseDto> _createOngoingCourseService;
        private readonly ILogger<CreateOngoingCourseServiceDecorator> _logger;

        public CreateOngoingCourseServiceDecorator(
            IService<CreateOngoingCourseParameter, OngoingCourseDto> createOngoingCourseService,
            ILogger<CreateOngoingCourseServiceDecorator> logger)
        {
            _createOngoingCourseService = createOngoingCourseService;
            _logger = logger;
        }

        public async Task<Result<OngoingCourseDto>> CallAsync(CreateOngoingCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _createOngoingCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<OngoingCourseDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
