﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.OngoingCourses
{
    public class UpdateOngoingCourseService : IService<UpdateOngoingCourseParameter, OngoingCourseDto>
    {
        private readonly IRepository<OngoingCourse> _ongoingCourseRepository;

        public UpdateOngoingCourseService(IRepository<OngoingCourse> ongoingCourseRepository)
        {
            _ongoingCourseRepository = ongoingCourseRepository;
        }

        public async Task<Result<OngoingCourseDto>> CallAsync(UpdateOngoingCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var ongoingCourseToUpdate = await _ongoingCourseRepository.GetAsync(parameter.OngoingCourse.Id);

            var ongoingCourseEntity = Convert(parameter.OngoingCourse, ongoingCourseToUpdate);

            var result = await _ongoingCourseRepository.UpdateAsync(ongoingCourseEntity);

            var ongoingCourseDto = Convert(result);

            return new Result<OngoingCourseDto>(200, ongoingCourseDto);
        }

        private OngoingCourse Convert(UpdateOngoingCourseDto ongoingCourseDto, OngoingCourse ongoingCourseEntity)
        {
            ongoingCourseEntity.StartDate = ongoingCourseDto.StartDate;
            ongoingCourseEntity.EndDate = ongoingCourseDto.EndDate;

            return ongoingCourseEntity;
        }

        private OngoingCourseDto Convert(OngoingCourse ongoingCourse)
        {
            return new OngoingCourseDto()
            {
                Id = ongoingCourse.Id,
                StartDate = ongoingCourse.StartDate,
                EndDate = ongoingCourse.EndDate
            };
        }
    }
}
