﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;
using DataAccess;
using DataAccess.Entities;
using System.Linq.Expressions;

namespace CoursesApi.Services.OngoingCourses
{
    public class ListOngoingCourseService : IService<ListOngoingCourseParameter, List<OngoingCourseDto>>
    {
        private readonly IRepository<OngoingCourse> _ongoingCoursesRepository;

        public ListOngoingCourseService(IRepository<OngoingCourse> ongoingCoursesRepository)
        {
            _ongoingCoursesRepository = ongoingCoursesRepository;
        }

        public async Task<Result<List<OngoingCourseDto>>> CallAsync(ListOngoingCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _ongoingCoursesRepository.ListAsync(
                parameter.Limit,
                null
            );

            var ongoingCourseDtos = result
                .Select(Convert)
                .ToList();

            return new Result<List<OngoingCourseDto>>(200, ongoingCourseDtos);
        }

        private OngoingCourseDto Convert(OngoingCourse ongoingCourse)
        {
            return new OngoingCourseDto()
            {
                Id = ongoingCourse.Id,
                StartDate = ongoingCourse.StartDate,
                EndDate = ongoingCourse.EndDate,
                Course = new Models.Courses.Dtos.CourseDto()
                {
                    Id = ongoingCourse.Course.Id,
                    Price = ongoingCourse.Course.Price,
                    Title = ongoingCourse.Course.Title,
                    Hours = ongoingCourse.Course.Hours,
                    Description = ongoingCourse.Course.Description
                }
            };
        }
    }
}
