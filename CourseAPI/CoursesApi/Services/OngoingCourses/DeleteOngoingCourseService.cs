﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Dtos;
using CoursesApi.Models.OngoingCourses.Parameters;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.OngoingCourses
{
    public class DeleteOngoingCourseService : IService<DeleteOngoingCourseParameter, OngoingCourseDto>
    {
        private readonly IRepository<OngoingCourse> _ongoingCourseRepository;

        public DeleteOngoingCourseService(IRepository<OngoingCourse> ongoingCourseRepository)
        {
            _ongoingCourseRepository = ongoingCourseRepository;
        }

        public async Task<Result<OngoingCourseDto>> CallAsync(DeleteOngoingCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _ongoingCourseRepository.DeleteAsync(parameter.Id);

            if (result)
            {
                return new Result<OngoingCourseDto>(200, new OngoingCourseDto());
            }

            return new Result<OngoingCourseDto>(404, null);
        }
    }
}
