﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Dtos;
using CoursesApi.Models.Lecturers.Parameters;
using DataAccess;
using DataAccess.Entities;

namespace CoursesApi.Services.Lecturers
{
    public class DeleteLecturerService : IService<DeleteLecturerParameter, LecturerDto>
    {
        private readonly IRepository<Lecturer> _lecturerRepository;

        public DeleteLecturerService(IRepository<Lecturer> lecturerRepository)
        {
            _lecturerRepository = lecturerRepository;
        }

        public async Task<Result<LecturerDto>> CallAsync(DeleteLecturerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _lecturerRepository.DeleteAsync(parameter.Id);

            if (result)
            {
                return new Result<LecturerDto>(200, new LecturerDto());
            }

            return new Result<LecturerDto>(404, null);
        }
    }
}
