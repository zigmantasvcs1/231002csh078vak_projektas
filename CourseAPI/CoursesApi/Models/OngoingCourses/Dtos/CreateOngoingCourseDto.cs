﻿namespace CoursesApi.Models.OngoingCourses.Dtos
{
    public class CreateOngoingCourseDto
    {
        public int CourseId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
