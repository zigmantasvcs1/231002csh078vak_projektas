﻿namespace CoursesApi.Models.OngoingCourses.Parameters
{
    public class GetOngoingCourseParameter
    {
        public GetOngoingCourseParameter(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("Bad id");
            }

            Id = id;
        }

        public int Id { get; }
    }
}
