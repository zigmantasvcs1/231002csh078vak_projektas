﻿namespace CoursesApi.Models.OngoingCourses.Parameters
{
    public class DeleteOngoingCourseParameter
    {
        public DeleteOngoingCourseParameter(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
