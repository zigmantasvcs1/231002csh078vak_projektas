﻿namespace CoursesApi.Models.Students.Parameters
{
    public class DeleteStudentParameter
    {
        public DeleteStudentParameter(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
