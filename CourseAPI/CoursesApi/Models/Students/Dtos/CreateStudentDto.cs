﻿namespace CoursesApi.Models.Students.Dtos
{
    public class CreateStudentDto
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime? BirthDay { get; set; }
    }
}
