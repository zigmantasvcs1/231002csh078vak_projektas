﻿namespace CoursesApi.Models.Lecturers.Dtos
{
    public class LecturerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string DocumentNumber { get; set; }
    }
}
