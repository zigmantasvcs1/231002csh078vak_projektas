﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
    public class OngoingCourseLine : BaseEntity
    {
        [Required]
        public int OngoingCourseId { get; set; }

        [Required]
        public int LecturerId { get; set; }

        [Required]
        public int StudentId { get; set; }

        [ForeignKey("OngoingCourseId")]
        public virtual OngoingCourse OngoingCourse { get; set; }

        [ForeignKey("LecturerId")]
        public virtual Lecturer Lecturer { get; set; }

        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
    }
}
